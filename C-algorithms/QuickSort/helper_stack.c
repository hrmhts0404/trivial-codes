typedef struct QSIntStack QSIntStack ;
struct QSIntStack {
  int *stack;
  int  height;
  int  capacity;
};

static QSIntStack initIntStack(int data[], int cap)
{
  QSIntStack obj;
  obj.stack = data;
  obj.height   = 0;
  obj.capacity = cap;

  return obj;
}

static void push(QSIntStack *p, int item) {
  // 速度のために height, cap の比較は行わない
  (p->stack)[ (p->height)++ ] = item ;
}

static int pop(QSIntStack *p) {
  // 速度のために height で条件分岐しない
  return (p->stack)[ --(p->height) ];
}