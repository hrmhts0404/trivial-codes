void QuickSort_int_naive(int target[], int start, int end);
int divide_naive(int target[], int start, int end);


void QuickSort_int_naive(int target[], int start, int end)
{
  if( start>=end ) return;
  
  int pivot_pos = divide_naive(target, start, end);
  QuickSort_int_naive(target, start,  pivot_pos-1);
  QuickSort_int_naive(target, pivot_pos+1, end);
}

int divide_naive(int target[], int start, int end)
{
  int pivot = target[start]; // 左端を pivot に
  int left  = start;
  int right = end+1;
  int tmp;

  for(;;) {

    // インデックスの移動
    // インデックスをずらす方向に Pivot (Sentinel)が有るか否かで
    // 反対のインデックスとの比較の必要性が出てくる
    // この比較をしないとインデックスのrangeを通り抜けて、
    // セグフォや場合によっては突然 0 がソート結果に出現したりする
    while( target[--right] > pivot ) ;
    while( (++left < right) && (target[left] < pivot) ) ;
    
    // 衝突判定
    // = がないとインデックスが行き過ぎて pivot_pos がずれることがある
    // 具体的には ソート幅が奇数で、ちょうど真ん中でかちあう時
    //　ループを抜けたあとのスワップでずれる
    if( left >= right ) break;
    
    // Swap (left,right)
    tmp = target[left];
    target[left] = target[right];
    target[right]= tmp;
  }

  // Swap (Pivot, right)
  // left ではいけない理由(rightのみが pivot_pos に適当な理由)
  // index移動と条件判定の順序から、
  // 片側のすべての値が pivot より {大き/小さ}い ことが保証されるのが
  // right のみだから
  tmp = target[right];
  target[right] = pivot;
  target[start] = tmp;

  return right;
}