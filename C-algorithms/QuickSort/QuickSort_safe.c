#include <math.h>

void QuickSort_int_safe(int[], int, int);

//helpers
#include "helper_stack.c"
static int divide_int_safe(int[], int, int);
static int isLeftLonger(int,int,int);

void QuickSort_int_safe(int target[], int start, int end)
{
  // スタックにスタックを用意
  const int stack_size = (int)( log2((double)(end-start+1)) + 1 )*2 ;
  int stack_data[stack_size] ;
  QSIntStack  obj   = initIntStack(stack_data, stack_size);
  QSIntStack *Stack = &obj;

  push(Stack, start);
  push(Stack, end);

  // ソート開始
  int pivot_pos;
  while(Stack->height > 0)
  {
    // 順序注意
    end   = pop(Stack);
    start = pop(Stack);

    for(;;)
    {
      if( start>=end ) break;      
      pivot_pos = divide_int_safe(target, start, end);
      if( isLeftLonger(pivot_pos,start,end) ){
        push(Stack, start);
        push(Stack, pivot_pos-1);
        start = pivot_pos+1; 
      }else{
        push(Stack, pivot_pos+1);
        push(Stack, end);
        end   = pivot_pos-1;
      }
    }

  }
}

static int divide_int_safe(int target[], int start, int end)
{
  int pivot = target[start]; // 左端を pivot に
  int left  = start;
  int right = end+1;
  int tmp;

  for(;;) {
    while( target[--right] > pivot ) ;
    while( (++left < right) && (target[left] < pivot) ) ;
    if( left >= right )
      break;
    // Swap (left,right)
    tmp = target[left];
    target[left] = target[right];
    target[right]= tmp;
  }

  // Swap (Pivot, right)
  tmp = target[right];
  target[right] = pivot;
  target[start] = tmp;

  return right;
}

static int isLeftLonger(int pos, int start, int end) {
  if (pos - start > end - pos) {
    return 1;
  }else{
    return 0;
  }
}