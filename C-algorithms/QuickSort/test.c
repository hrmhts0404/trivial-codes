#include <stdio.h>

#include "QuickSort_naive.c"
#include "QuickSort_safe.c"
#include "QuickSort_final.h"

#define TRUE  0 
#define FALSE 1

int  is_Sorted(int before[], int target[], int arr_size);
void randomize(int before[], int target[], int arr_size);

void demo(void){
  printf("== Working Demo ==\n");
  int target[32]; int i;
  randomize(target, target, 32);
  printf("  Before:\n  ");
  for(i=0; i<32; ++i) printf("%d ", target[i]);
  printf("\n  After:\n  ");
  QuickSort_int(target, 0, 31);
  for(i=0; i<32; ++i) printf("%d ", target[i]);
  printf("\n");
}

int main(void){

  // 全射か否かを調べるため
  // (初期化がランダムなため、単射性を調べるのは大変な割にリターンがない)
  // before と target を同じ配列にして
  // target のみソートして比較する
  int before16[16];
  int target16[16];

  int before32[32];
  int target32[32];

  int before256[256];
  int target256[256];

  int result;


  // Test QuickSort_naive
  result = 0;
  randomize(before16, target16, 16);
  randomize(before32, target32, 32);
  QuickSort_int_naive(target16, 0, 15);
  QuickSort_int_naive(target32, 0, 31);

  result += is_Sorted(before16, target16, 16);
  result += is_Sorted(before32, target32, 32);

  if (result == TRUE){
    printf("Test QuickSort_int_naive...[ OK! ](2/2)\n");
  }else{
    printf("Test QuickSort_int_naive...[ Fail](%d/2)\n", (2-result));
  }


  // Test QuickSort_safe
  result = 0;
  randomize(before16, target16, 16);
  randomize(before32, target32, 32);
  randomize(before256,target256,256);
  QuickSort_int_safe(target16, 0, 15);
  QuickSort_int_safe(target32, 0, 31);
  QuickSort_int_safe(target256,0,255);

  result += is_Sorted(before16, target16, 16);
  result += is_Sorted(before32, target32, 32);
  result += is_Sorted(before256,target256,256);

  if (result == TRUE){
    printf("Test QuickSort_int_safe ...[ OK! ](3/3)\n");
  }else{
    printf("Test QuickSort_int_safe ...[ Fail](%d/3)\n", (3-result));
  }


  // Test QuickSort_safe
  result = 0;
  randomize(before16, target16, 16);
  randomize(before32, target32, 32);
  randomize(before256,target256,256);
  QuickSort_int(target16, 0, 15);
  QuickSort_int(target32, 0, 31);
  QuickSort_int(target256,0,255);

  result += is_Sorted(before16, target16, 16);
  result += is_Sorted(before32, target32, 32);
  result += is_Sorted(before256,target256,256);

  if (result == TRUE){
    printf("Test QuickSort_int      ...[ OK! ](3/3)\n");
  }else{
    printf("Test QuickSort_int      ...[ Fail](%d/3)\n", (3-result));
  }

  demo();

  return 0;
}

void randomize(int before[], int target[],int size){
  int r;
  int i = 0;
  while( i<size ){
    r = (rand() - rand()/2) % 64;
    before[i] = r;
    target[i] = r;
    ++i;
  }
}
int is_InOrder(int target[], int size);
int is_Surject(int before[], int target[], int size);
int is_Sorted(int before[], int target[], int size){
  return (
    is_InOrder(target, size) + is_Surject(before, target, size)
    );
}
int is_InOrder(int arr[], int size){
  int lim = size-1;
  int missed = 0;
  int i = 0;
  while( i<lim ){
    if( !(arr[i]<=arr[++i]) )
      ++missed;
  }
  if (missed == 0)
    return TRUE;
  return FALSE;
}
int is_Surject(int before[], int target[], int size) {
  // 厳密に全射を判定する関数ではない
  // あくまで「全射でないことはない」程度
  int domain, found;
  int i, j;
  
  for(i=0; i<size; ++i){
    domain = before[i];
    found  = FALSE;
    for(j=0; j<size; ++j)
      if(domain == target[j]) found = TRUE;
    if (found == FALSE)
      return FALSE;
  }

  return TRUE;
}