#include "QuickSort_final.h"


// Helpers.
#include "helper_stack.c"
static void QuickSort_int_naive(int[],int,int);
static int  QS_divide(int[],int,int);
static void QS_median_to_left(int[],int,int);


void QuickSort_int(int target[], int start, int end)
{
  const int stack_size = (int)( log2((double)(end-start+1)) + 1 )*2 ;
  int stack_data[stack_size] ;
  QSIntStack  obj   = initIntStack(stack_data, stack_size);
  QSIntStack *Stack = &obj;

  push(Stack, start);
  push(Stack, end);

  int pivot_pos;
  while(Stack->height > 0)
  {
    end   = pop(Stack);
    start = pop(Stack);

    QS_median_to_left(target, start, end);   

    for(;;)
    {
      if( start>=end ) break;      

      pivot_pos = QS_divide(target, start, end);
      
      if( pivot_pos-start > end-pivot_pos ){
        push(Stack, start);
        push(Stack, pivot_pos-1);
        start = pivot_pos+1;
      }else{
        push(Stack, pivot_pos+1);
        push(Stack, end);
        end   = pivot_pos-1;
      }
      
      if( (end-start)<16 ){
        // range が短い場合、そのまま最後までやり切る
        QuickSort_int_naive(target, start, end);
        break;
      }
    }
  
  }

}

static void QuickSort_int_naive(int target[], int start, int end)
{ // スタックサイズを気にしない実装
  if( start>=end ) return;
  int pivot_pos = QS_divide(target, start, end);
  QuickSort_int_naive(target, start,  pivot_pos-1);
  QuickSort_int_naive(target, pivot_pos+1, end);
}

static int QS_divide(int target[], int start, int end)
{
  int pivot = target[start];
  int left  = start;
  int right = end+1;
  int tmp;

  for(;;) {
    while( target[--right] > pivot ) ;
    while( (++left < right) && (target[left] < pivot) ) ;
    
    if( left >= right ) break;

    tmp = target[left];
    target[left] = target[right];
    target[right]= tmp;
  }
  tmp = target[right];
  target[right] = pivot;
  target[start] = tmp;

  return right;
}

static void QS_median_to_left(int target[], int start, int end)
{
  int left   = target[start]        ;
  int middle = target[(start+end)/2];
  int right  = target[end]          ;
  // swap のため
  int tmp    = target[start];
  if(left<=right){
    if(right<=middle){
      // right is the median.
      target[start] = right;
      target[end]   = tmp;
    }else{
      if(middle>left){
        // middle is the median.
        target[start] = middle;
        target[(start+end)/2] = tmp;
      }
      // else. left is the median. do nothing.
    }
  }else{
    // right < left
    if(middle <= right){
      // right is the median.
      target[start] = right;
      target[end]   = tmp;
    }else{
      if(middle < left){
        // middle is the median.
        target[start] = middle;
        target[(start+end)/2] = tmp;
      }
    }
    // else. left is the median. do nothing.
  } 
}